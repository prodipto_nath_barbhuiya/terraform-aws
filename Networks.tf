resource "aws_vpc" "Demo-VPC" {
  cidr_block      = "10.9.0.0/19"
 instance_tenancy = "default"
 tags = {
    Name        = "Project01"
    creator     = "Prodipto_Nath"
    region      = "us_east"
    environment = "Testing"
   }
}
output "vpc_id" {
  value = aws_vpc.Demo-VPC.id
}

resource "aws_subnet" "Demo-subnet" {
  vpc_id     = aws_vpc.Demo-VPC.id
  cidr_block = "10.9.0.0/24"
  availability_zone = "us-east-1a"

  tags = {
     Name        = "Project01"
    creator     = "Prodipto_Nath"
    region      = "us_east"
    environment = "Testing"
  }
}
