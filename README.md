New Project for Terraform CI/CD 


1) To implement the orchestration with #GITLAB and #TERRAFORM and provision the AWS resource with CI/CD pipeline.

2) To manage Terraform state, Remote backends allow you to store the state file in a remote, shared store. Remote backends are supported by Amazon S3

a) create an S3 bucket.
b) DynamoDB for locking with Terraform.
c) add a backend configuration in Terraform code.

